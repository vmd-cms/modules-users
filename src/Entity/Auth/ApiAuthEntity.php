<?php

namespace VmdCms\Modules\Users\Entity\Auth;

use VmdCms\Modules\Users\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use VmdCms\CoreCms\Exceptions\Api\Auth\ApiAuthException;
use VmdCms\Modules\Users\Models\UserSession;

class ApiAuthEntity
{
    /**
     * @return User|null
     */
    public static function getAuthUser(): ?User
    {
        return auth()->guard('api_user')->user();
    }

    /**
     * @return int|null
     */
    public static function getAuthId(): ?int
    {
        return auth()->guard('api_user')->id();
    }

    /**
     * @param User $user
     */
    public static function authUser(User $user)
    {
        auth()->guard('api_user')->login($user,true);
    }

    /**
     * @param string|null $apiToken
     * @throws ApiAuthException
     */
    public static function authUserApiToken(string $apiToken = null)
    {
        static::authUser(static::getUserByToken($apiToken));
    }

    /**
     * @param string|null $apiToken
     * @return User
     * @throws ApiAuthException
     */
    public static function getUserByToken(string $apiToken = null): User
    {
        $session = static::getUserSessionByToken($apiToken);
        $user = $session->user;
        if(!$user instanceof User) {
            throw new ApiAuthException('user_not_auth');
        }

        if(!$user->active || $user->deleted) {
            throw new ApiAuthException('user_suspended');
        }

        $session->requests += 1;
        $session->save();

        return $user;
    }

    /**
     * @param string $apiToken
     * @return UserSession
     * @throws ApiAuthException
     */
    public static function getUserSessionByToken(string $apiToken = null): UserSession
    {
        if(empty($apiToken)){
            throw new ApiAuthException('empty_api_token');
        }

        $session = UserSession::where('token',$apiToken)->with('user')
            ->where('active',true)->first();

        if(!$session instanceof UserSession){
            throw new ApiAuthException('user_not_auth');
        }

        if($session->expires < date('Y-m-d H:i:s')){
            $session->active = false;
            $session->save();
            throw new ApiAuthException('user_token_expired');
        }

        return $session;
    }

    /**
     * @param User $user
     * @return UserSession
     * @throws ApiAuthException
     */
    public static function createSession(User $user): UserSession{
        try {
            $token = hash('sha256', Str::random(60));
            $session = new UserSession();
            $session->user_id = $user->id;
            $session->ip = \request()->getClientIp();
         //   $session->session_id = \VmdCms\CoreCms\Services\Auth\UserSession::getUserSessionId();
            $session->token = $token;
            $session->expires = Carbon::now()->addDays(30)->format('Y-m-d H:i:s');
            $session->save();
        }catch (\Exception $exception){
            throw new ApiAuthException();
        }
        return $session;
    }

    public static function suspendSessions(User $user){
        try {
            UserSession::where('user_id',$user->id)->update(['active'=>false]);
        }catch (\Exception $exception){
            throw new ApiAuthException();
        }
    }

}
