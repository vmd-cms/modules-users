<?php

namespace VmdCms\Modules\Users\Entity\Auth;

use App\Modules\Content\Services\DataShare;
use App\Modules\Services\TurboSms\Entity\TurboSmsEntity;
use App\Modules\Users\Exceptions\SignUpException;
use App\Modules\Users\Models\Components\AuthTranslate;
use App\Modules\Users\Models\User;
use App\Modules\Users\Models\UserConfirmation;
use App\Modules\Users\Models\UserGroup;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\Services\Logger;
use VmdCms\Modules\Users\Enums\UserGroupEnums;
use VmdCms\Modules\Users\Services\CoreEventEnums;

class ConfirmationEntity
{
    protected $logger;

    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger ?? new Logger('users');
    }

    /**
     * @param string $value
     * @param string $key
     * @return UserConfirmation
     * @throws SignUpException
     */
    protected function getOrCreateConfirmation(string $value, string $key = UserConfirmation::EMAIL)
    {
        $confirmation = UserConfirmation::where('value',$value)
            ->where('key',$key)->first();

        if($confirmation instanceof UserConfirmation) {
            $this->validateLastConfirmation($confirmation);
        }
        else {
            $confirmation = new UserConfirmation();
            $confirmation->key = $key;
            $confirmation->value = $value;
        }
        return $confirmation;
    }

    /**
     * @param UserConfirmation $confirmation
     * @throws SignUpException
     */
    protected function validateLastConfirmation(UserConfirmation $confirmation)
    {
        $datashare = DataShare::getInstance(false);
        $datashare->translates->appendGroups([AuthTranslate::getModelGroup()]);
        if($confirmation->activated){
            throw new SignUpException($datashare->getTranslate('current_number_confirmed'));
        }

        $secondsPast = (Carbon::now())->diffInSeconds($confirmation->updated_at);
        if($secondsPast < 30){
            throw new SignUpException($datashare->getTranslate('retry_later'));
        }
    }


    /**
     * @param string $phone
     * @param string $code
     * @return UserConfirmation
     * @throws SignUpException
     */
    protected function getUserConfirmation(string $phone,string $code): UserConfirmation
    {
        try {
            $confirmation = UserConfirmation::where('key',UserConfirmation::PHONE)
                ->where('code',$code)->where('value',$phone)->first();

            if(!$confirmation) {
                throw new SignUpException('UserConfirmation not found');
            }

            if($confirmation->activated){
                throw new SignUpException('User confirmation is already activated');
            }

        }catch (\Exception $exception){
            $this->logger->error('Exception in ' . __METHOD__);
            $this->logger->error('Params ' . json_encode(['code' => $code, 'phone' => $phone]));
            $this->logger->error($exception->getMessage());
            $datashare = DataShare::getInstance(false);
            $datashare->translates->appendGroups([AuthTranslate::getModelGroup()]);
            throw new SignUpException($datashare->getTranslate('error_confirm_registration'));
        }
        return $confirmation;
    }

    /**
     * @param string $phone
     * @param string $email
     * @throws SignUpException
     */
    protected function checkIsExistUser(string $phone,string $email){
        try {

            $user = User::where('email',$email)
                ->orWhere('phone',$phone)->first();

            if($user) {
                throw new SignUpException('User with this credentials exist');
            }

        }catch (\Exception $exception){
            $this->logger->error('Exception in ' . __METHOD__);
            $this->logger->error('Params ' . json_encode(['email' => $email, 'phone' => $phone]));
            $this->logger->error($exception->getMessage());
            $datashare = DataShare::getInstance(false);
            $datashare->translates->appendGroups([AuthTranslate::getModelGroup()]);
            throw new SignUpException($datashare->getTranslate('error_email_or_phone_exist'));
        }
    }

}
