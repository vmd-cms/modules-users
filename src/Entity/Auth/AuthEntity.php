<?php

namespace VmdCms\Modules\Users\Entity\Auth;

use App\Modules\Users\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\Exceptions\Api\Auth\ApiAuthException;
use VmdCms\CoreCms\Services\Config;
use VmdCms\Modules\Users\Services\CoreEventEnums;

class AuthEntity
{
    /**
     * @return User|null
     */
    public static function getAuthUser(): ?User
    {
        return auth()->guard('user')->user();
    }

    /**
     * @return int|null
     */
    public static function getAuthId(): ?int
    {
        return auth()->guard('user')->id();
    }

    /**
     * @param User $user
     */
    public static function authUser(User $user)
    {
        auth()->guard('user')->login($user,true);
    }

    /**
     * @param string $email
     * @throws ApiAuthException
     */
    public function recoveryPassword(string $email)
    {
        try {
            if(!$user = User::where('email',$email)->first())
            {
                throw new ApiAuthException('User not found');
            }
            $date = date('Y-m-d H:i:s');
            $token = str_replace(['/','%'],['-','-'],Hash::make($email . $date));

            DB::beginTransaction();
            DB::table('users_password_resets')->where('email',$email)->delete();
            DB::table('users_password_resets')->insert([
                'email' => $email, 'token' => $token, 'created_at' => $date
            ]);

            Event::dispatch(CoreEventEnums::USER_RECOVERY_PASSWORD_EMAIL,[
                'email' => $email,
                'token' => $token,
                'user_id' => $user->id,
                'user_email' => $email,
                'link' => Config::getInstance()->getFrontendAppUrl() . '/oauth/recovery/' . $token
            ]);

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new ApiAuthException(trans('passwords.user'));
        }
    }

    /**
     * @param string $token
     * @throws ApiAuthException
     */
    public function oauthRecoveryToken(string $token)
    {
        try {
            DB::beginTransaction();

            $result = DB::table('users_password_resets')
                ->select('users_password_resets.*')
                ->where('token',$token)->first();

            if(empty($result)){
                throw new ApiAuthException();
            }

            DB::table('users_password_resets')->where('token',$token)->delete();

            $user = User::where('email',$result->email)->first();

            Event::dispatch(CoreEventEnums::USER_RECOVERY_PASSWORD_EMAIL_CONFIRMED,[
                'token' => $token,
                'user_id' => $user->id,
                'user_email' => $user->email
            ]);

            static::authUser($user);

            Event::dispatch(CoreEventEnums::USER_SIGN_IN,['is_auto_sign_in' => true]);

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw new ApiAuthException();
        }
    }
}
