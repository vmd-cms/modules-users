<?php

namespace VmdCms\Modules\Users\Entity\Cabinet;

use App\Modules\Content\Services\DataShare;
use App\Modules\Translates\Models\Groups\FormGroupTranslate;
use App\Modules\Users\DTO\UserDTO;
use App\Modules\Users\Models\Components\CabinetTranslate;
use App\Modules\Users\Services\UserRouter;
use Illuminate\Routing\Route;
use VmdCms\CoreCms\Collections\NavsCollection;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\DTO\NavDTO;
use VmdCms\CoreCms\Services\BreadCrumbs;
use VmdCms\Modules\Users\Models\User;

class CabinetEntity
{

    public function sharePageData(Route $route)
    {
        $dataShareInstance = DataShare::getInstance();
        $this->shareUserDTO();

        $dataShareInstance->translates->appendGroups([CabinetTranslate::getModelGroup(),FormGroupTranslate::getModelGroup()]);

        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto(route(UserRouter::ROUTE_CABINET,[],false),$dataShareInstance->getTranslate('cabinet')));
        $this->shareContentDataByRouteName($route->getName());

        $navs = new NavsCollection();
        $navs->append(new NavDTO($dataShareInstance->getTranslate('cabinet_data'), $this->getCabinetLink(UserRouter::ROUTE_CABINET,$route)));

        $dataShareInstance->appendData('cabinetNavs',$navs->getItems());
        $dataShareInstance->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());
    }

    protected function getCabinetLink(string $routeName, Route $route)
    {
        return $route->getName() !== $routeName ? route($routeName,[],false) : '#';
    }

    protected function shareCabinetData(){
        $this->appendContentViewPath('content::layouts.components.cabinet.data_block');
    }

    protected function shareContentDataByRouteName(string $routeName)
    {
        switch ($routeName){
            default: $this->shareCabinetData();
        }
    }

    protected function appendContentViewPath(string $path)
    {
        DataShare::getInstance()->appendData('contentViewPath',$path);
    }

    protected function shareUserDTO()
    {
        $user = auth()->guard('user')->user();
        if(!$user instanceof User) abort(403);
        DataShare::getInstance()->appendData('user',new UserDTO($user));
    }
}
