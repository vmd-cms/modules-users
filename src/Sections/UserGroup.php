<?php

namespace VmdCms\Modules\Users\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;

class UserGroup extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'user_groups';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('customers_groups');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('info.title',CoreLang::get('title')),
            Column::text('active'),
            Column::date('created_at')->setFormat('Y-m-d'),
        ])->setSearchable(true);
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('slug')->unique(),
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::switch('active'),
        ]);
    }
    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\UserGroup::class;
    }
}
