<?php

namespace VmdCms\Modules\Users\Sections\Components;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class AuthTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'auth_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\Components\AuthTranslate::class;
    }
}
