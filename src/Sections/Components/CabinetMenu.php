<?php

namespace VmdCms\Modules\Users\Sections\Components;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\Content\Sections\Menu\Menu;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class CabinetMenu extends Menu
{
    /**
     * @var string
     */
    protected $slug = 'cabinet_menu';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('cabinet_menu');
    }

    /**
     * @return DisplayInterface
     */
    public function display()
    {
        return Display::dataTable([
            ColumnEditable::text('info.title',CoreLang::get('title'))->maxLength(10),
            ColumnEditable::text('url')->unique(),
            ColumnEditable::switch('active')->alignCenter(),
        ])->setSearchable(true)->orderable();
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::radio('active'),
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','URL')->setDependedField('info.title')->required()->unique(),
        ])->setHeadTitle(CoreLang::get('editing_menu') . ' ', 'info.title');
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\Components\CabinetMenu::class;
    }
}
