<?php

namespace VmdCms\Modules\Users\Sections\Components\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;

class SignUpInfoBlock extends \VmdCms\Modules\Users\Sections\Components\AuthGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key',)->setDisabled(true),
            FormComponent::switch('active',),
            FormComponent::input('title',),
            FormComponent::input('link'),
            FormComponent::input('link_title',CoreLang::get('text')),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\Components\Blocks\SignUpInfoBlock::class;
    }
}
