<?php

namespace VmdCms\Modules\Users\Sections\Components\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\Modules\Users\Sections\Components\AuthGroupBlock;

class LogInInfoBlock extends AuthGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key')->setDisabled(true),
            FormComponent::switch('active'),
            FormComponent::text('title'),
            FormComponent::input('link'),
            FormComponent::input('link_title',CoreLang::get('text')),
            FormComponent::datatable('children', CoreLang::get('benefits'))->setComponents([
                FormTableColumn::input('title',CoreLang::get('property')),
                FormTableColumn::switch('active'),
            ])->setIsDeletable(true)->setPaddingTop(100)->displayRow(),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\Components\Blocks\LogInInfoBlock::class;
    }

}

