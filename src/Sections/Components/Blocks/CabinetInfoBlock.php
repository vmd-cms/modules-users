<?php

namespace VmdCms\Modules\Users\Sections\Components\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\Users\Sections\Components\CabinetGroupBlock;

class CabinetInfoBlock extends CabinetGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key')->setDisabled(true),
            FormComponent::switch('active'),
            FormComponent::input('description'),
            FormComponent::input('title'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\Components\Blocks\CabinetInfoBlock::class;
    }

}

