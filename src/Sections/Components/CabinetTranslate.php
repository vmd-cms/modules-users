<?php

namespace VmdCms\Modules\Users\Sections\Components;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class CabinetTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'cabinet_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\Components\CabinetTranslate::class;
    }
}
