<?php

namespace VmdCms\Modules\Users\Sections;

use VmdCms\CoreCms\Collections\DependedComponentCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterComponent;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCancelButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCustomButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveButton;
use VmdCms\CoreCms\Dashboard\Forms\Components\ColumnComponent;
use VmdCms\CoreCms\Dashboard\Forms\Components\DependedComponent;
use VmdCms\CoreCms\DTO\Dashboard\DependComponentDto;
use VmdCms\CoreCms\DTO\Dashboard\Navs\ChipItemDto;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Traits\Dashboard\Filters\HasConditionFilter;
use VmdCms\Modules\Orders\Models\Order;
use VmdCms\Modules\Orders\Models\OrderItem;
use VmdCms\Modules\Orders\Models\Params\DeliveryTypeParam;
use VmdCms\Modules\Users\DTO\UserDeliveryDTO;
use VmdCms\Modules\Users\Models\UserDelivery;

class User extends CmsSection
{
    use HasConditionFilter;

    /**
     * @var string
     */
    protected $slug = 'users';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('customers');
    }

    public function getChip(): ?ChipItemDto
    {
        return new ChipItemDto($this->cmsModelClass::count(),'#F7685B');
    }

    public function display()
    {
        $filter = (new FilterDisplay())
            ->appendFilterComponent($this->getConditionFilterComponent(new \VmdCms\Modules\Users\Models\User()));

        $displayColumns = [
            Column::text('id','#')->setWidth(25)->alignCenter(),
            Column::custom('full_name')->setContentCallback(function ($model){
                return '<div>'.$model->full_name.'</div>
                        <div class="pt-1"><i class="icon icon-message pr-1"></i>'.$model->email.'</div>
                        <div class="pt-1"><i class="icon icon-phone pr-1"></i>'.$model->phone.'</div>';
            })->setWidth(200)->setSearchableCallback(function ($query,$search){
                $query->orWhere('email','like','%' . $search . '%')
                    ->orWhere('phone','like','%' . $search . '%')
                    ->orWhere('first_name','like','%' . $search . '%')
                    ->orWhere('last_name','like','%' . $search . '%')
                    ->orWhere('father_name','like','%' . $search . '%');
            })->setSortableCallback(function ($query,$sortType){
                $query->orderBy('first_name', $sortType)
                    ->orderBy('last_name', $sortType)
                    ->orderBy('email', $sortType)
                    ->orderBy('phone', $sortType);
            })
        ];

        if(class_exists("\App\Modules\Orders\Entity\OrderEntity")){
            $displayColumns[] =  Column::text('total_orders_sum', CoreLang::get('sum_orders'))
                ->setValueCallback(function ($model){
                    return (new \VmdCms\Modules\Orders\Entity\OrderEntity())->getUserTotalOrdersSum($model);
                })
                ->setSortableCallback(function ($query,$sortType){
                    $query->selectRaw('users.*, sum(' . OrderItem::table() . '.total) as total_orders_sum')
                        ->leftJoin(Order::table(),function ($join){
                            $join->on(Order::table() . '.user_id' , '=' , 'users.id');
                        })
                        ->leftJoin(OrderItem::table(),function ($join){
                            $join->on(OrderItem::table() . '.order_id' , '=' , Order::table() . '.id');
                        })
                        ->orderBy('total_orders_sum', $sortType)->groupBy('users.id');
                })->alignCenter();

            $displayColumns[] = Column::text('total_orders_count', CoreLang::get('quantity_orders'))
                ->setValueCallback(function ($model){
                    return (new \VmdCms\Modules\Orders\Entity\OrderEntity())->getUserTotalOrdersCount($model);
                })
                ->setSortableCallback(function ($query,$sortType){
                    $query->selectRaw('users.*, count(' . Order::table() . '.id) as total_orders_count')
                        ->leftJoin(Order::table(),function ($join){
                            $join->on(Order::table() . '.user_id' , '=' , 'users.id');
                        })
                        ->orderBy('total_orders_count', $sortType)->groupBy('users.id');
                })->alignCenter();
        }

        $displayColumns[] =  ColumnEditable::switch('active')->alignCenter();
        $displayColumns[] =  Column::date('created_at', CoreLang::get('registered'))->setFormat('Y-m-d')->alignCenter();

        return Display::dataTable($displayColumns)->setSearchable(true)->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->setFilter($filter);
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        if(parent::isDeletable($model)){
            return request()->route()->getName() != CoreRouter::ROUTE_RESTORE ? ($model && !$model->deleted) : true;
        }
        return false;
    }


    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {

        $this->setTitle(CoreLang::get('create_customer'));

        $columnProfileData = [
            FormComponent::input('full_name')
                ->displayRow()
                ->outlined(false)
                ->addClass('pt-10','container')
                ->addClass('max-width-250px','component_container')
                ->addClass('fs-18px weight-700 pt-0','component'),
            FormComponent::switch('confirmed','')
                ->setShowLabel(false)
                ->addClass('pb-2')
                ->addClass('fs-12px light weight-500 d-flex justify-start align-center','component_container')
                ->addClass('pl-2 mt-0 pt-0 height-24px','component')
                ->setTopHelpText(CoreLang::get('approved') . ':'),
            FormComponent::input('discount_percent','')
                ->setDefault(0)->float()->max(100)->min(0)
                ->setShowLabel(false)
                ->outlined(false)
                ->addClass('pb-2')
                ->addClass('fs-12px light weight-500 d-flex justify-start align-center','component_container')
                ->addClass('max-width-30px pl-2 mt-0 pt-0 height-30px','component')
                ->setTopHelpText(CoreLang::get('personal_discount') . ':')
                ->setBottomHelpText('%'),
            FormComponent::input('email','')
                ->email()
                ->unique()
                ->setShowLabel(false)
                ->outlined(false)
                ->addClass('pb-3')
                ->addClass('max-width-250px fs-14px weight-500 d-flex justify-start align-center','component_container')
                ->addClass('pl-2 mt-0 pt-0 height-35px','component')
                ->setTopHelpText('Email:'),
            FormComponent::phone('phone','')
                ->unique()
                ->setShowLabel(false)
                ->outlined(false)
                ->addClass('pb-3')
                ->addClass('max-width-250px fs-14px weight-500 d-flex justify-start align-center','component_container')
                ->addClass('pl-2 mt-0 pt-0 height-35px','component')
                ->setTopHelpText(CoreLang::get('phone') . ':'),
            FormComponent::input('city','')
                ->maxLength(50)
                ->setShowLabel(false)
                ->outlined(false)
                ->addClass('pb-3')
                ->addClass('max-width-250px fs-12px light weight-500 d-flex justify-start align-center','component_container')
                ->addClass('pl-2 mt-0 pt-0 height-30px','component')
                ->setTopHelpText(CoreLang::get('city') . ':'),
            FormComponent::password('PasswordHash','')
                ->maxLength(100)
                ->setShowLabel(false)
                ->outlined(false)
                ->addClass('pb-3')
                ->addClass('max-width-250px fs-12px light weight-500 d-flex justify-space-between align-center','component_container')
                ->addClass('pl-2 mt-0 pt-0 height-30px max-width-150px','component')
                ->setTopHelpText('Пароль:'),
            FormComponent::switch('active')->setDefault(true)->displayRow(),
            FormComponent::text('comments',CoreLang::get('comments'))->displayRow()
                ->addClass('max-width-250px','component_container')
        ];

        return Form::panel($columnProfileData)->addClass('pa-10');
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {

        $model = $this->getCmsModel()::find($id);

        $this->setTitle($model->full_name);

        $headPanel = FormComponent::row('')
            ->appendColumn((new ColumnComponent(9))->appendComponents([
                FormComponent::html('<span class="light">' . CoreLang::get('registered') . ' ' . date('d.m.Y',strtotime($model->created_at)) .' '. date('H:i:s',strtotime($model->created_at)) .' </span>')
                    ->setPaddingTop(10),
            ]))
            ->appendColumn((new ColumnComponent(3))->appendComponents([
                FormComponent::select('active', '')->justify('end')
                    ->setEnumValues(['0'=>CoreLang::get('turn_off'),'1'=>CoreLang::get('turn_on')])
                    ->addClass('ml-5 max-width-100px','component_container')
                    ->setShowLabel(false)->hideDetails(),
            ]));

        $columnProfileData = [
            FormComponent::html('<h2>'. $model->full_name .' </h2>')->addClass('pb-5'),
            FormComponent::html('<span class="light">' . CoreLang::get('ip_address') . ':</span><span>' . ($model->ip_address ?? '') . '</span>')->addClass('pb-2'),
//            FormComponent::html('<span class="light">Бонусы:</span><span>-2 грн на ед. тов.</span>')->addClass('pb-5'),
//            FormComponent::html('<h3>Адресс доставки</h3>')->addClass('pt-5'),
            FormComponent::text('comment',CoreLang::get('comments'))->displayRow()->addClass('max-width-300px'),
        ];

        $columnProfileButtons = [
            FormComponent::buttons('')->appendButtons(
                (new FormCustomButton(CoreLang::get('save')))->setAction(FormButton::ACTION_SAVE)->setIconClass('icon-edit')),
            FormComponent::buttons('')->appendButtons(
                (new FormCustomButton(CoreLang::get('change_password')))->setIconClass('icon-security')),

        ];

        if($model->deleted){
            $columnProfileButtons[] =  FormComponent::buttons('')->appendButtons(
                (new FormCustomButton(CoreLang::get('restore')))
                    ->setAction(FormButton::ACTION_RESTORE)
                    ->setRoutePath(CoreRouter::getRestoreRoute($this->getSectionSlug(),$id))
                    ->setIconClass('icon-off'));
        }else{
            $columnProfileButtons[] =  FormComponent::buttons('')->appendButtons(
                (new FormCustomButton(CoreLang::get('delete')))
                    ->setAction(FormButton::ACTION_DELETE)
                    ->setRoutePath(CoreRouter::getDeleteRoute($this->getSectionSlug(),$id))
                    ->setIconClass('icon-delete'));
        }

        $columnProfilePreviewComponents = [
            FormComponent::view('content::admin.sections.profile_preview')->wide(),
            FormComponent::view('content::admin.sections.profile_order_stat')->borderTop()->wide(),
        ];

        if(class_exists(\VmdCms\Modules\Orders\Models\Order::class)){
            $filter = (new FilterDisplay())
                ->appendFilterComponent((new FilterComponent('info.title'))
                    ->setCountCallback(function ($model) use($id) {
                        return \VmdCms\Modules\Orders\Models\Order::where('status_id', $model->id)
                            ->where('user_id',$id)->count();
                    })
                    ->setChipColorCallback(function ($model) {
                        return $model->color;
                    })
                    ->setHeadTitle(CoreLang::get('statuses_orders'))
                    ->setExpansionTitle(CoreLang::get('all_statuses'))
                    ->setFilterSlug('order_status_param')
                    ->setModelClass(\VmdCms\Modules\Orders\Models\Params\OrderStatusParam::class)
                    ->setFilterField('status_id')
                );

            $orderSection = new \VmdCms\Modules\Orders\Sections\Order();
            $orderSection->setCreatable(false);
            $display = $orderSection->display()->setSection($orderSection)->setWhereQuery(['user_id','=',$id]);

            $this->sectionPanels = [
                $display->getSectionPanel()->setColsMd(12)
            ];
        }

        $formPanel = Form::panel([
            FormComponent::row('')
                ->appendColumn(
                    (new ColumnComponent(8))->appendComponents([
                        $headPanel,
                        FormComponent::row('')
                            ->appendColumn((new ColumnComponent(9))->appendComponents($columnProfileData))
                            ->appendColumn((new ColumnComponent(3))->appendComponents($columnProfileButtons)),
                    ])->addClass('pl-10')
                )->appendColumn(
                    (new ColumnComponent(4))->appendComponents($columnProfilePreviewComponents)->borderLeft()
                ),
        ])->hideFloatActionGroup()->hideFooterActionGroup()->wide();

        if(isset($filter)){
            $formPanel->setFilter($filter);
        }

        return $formPanel;
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Users\Models\User::class;
    }

    protected function getDeliveryComponent(\VmdCms\Modules\Users\Models\User $user)
    {
        $orderParamDelivery = DeliveryTypeParam::where('slug','novaposhta')->first();
        $orderParamDeliveryId = $orderParamDelivery instanceof DeliveryTypeParam ? $orderParamDelivery->id : null;

        if(empty($orderParamDeliveryId)){
            return FormComponent::select('delivery')
                ->setDisplayField('info.title')
                ->displayRow()->addClass('pb-5');
        }

        $userDelivery = $user->userDelivery->where('order_param_id',$orderParamDeliveryId)->first();
        $userDeliveryDTO = $userDelivery instanceof UserDelivery ? new UserDeliveryDTO($userDelivery) : null;
        $dependedCollection = new DependedComponentCollection();
        $dependedCollection->appendItem(new DependComponentDto(
            $orderParamDeliveryId,
            FormComponent::view('')
                ->setViewPath('vmd_cms::admin.services.novaposhta.wrapper')
                ->setData(['userDeliveryDTO' => $userDeliveryDTO])
        ));

        return FormComponent::dependedSelect('delivery')
            ->setValueCallback(function () use($user) {
                $activeDelivery = UserDelivery::where('user_id',$user->id)->where('active',1)->first();
                return $activeDelivery instanceof UserDelivery ? $activeDelivery->order_param_id : null;
            })
            ->setStoreCallback(function ($model) use($orderParamDeliveryId){
                $selectedDeliveryId = request()->get('delivery') ? intval(request()->get('delivery')) : null;
                $userDeliveries = UserDelivery::where('user_id',$model->id)->get();
                if(is_countable($userDeliveries)){
                    foreach ($userDeliveries as $delivery){
                        $delivery->active = false;
                        $delivery->save();
                    }
                }

                $currentDelivery = UserDelivery::where('user_id',$model->id)->where('order_param_id',$selectedDeliveryId)->first();
                if(!$currentDelivery instanceof UserDelivery)
                {
                    $currentDelivery = new UserDelivery();
                    $currentDelivery->user_id = $model->id;
                    $currentDelivery->order_param_id = $selectedDeliveryId;
                }
                $currentDelivery->active = true;

                if($selectedDeliveryId == $orderParamDeliveryId)
                {
                    $currentDelivery->city_title = request()->get('delivery_city_title');
                    $currentDelivery->city_ref = request()->get('delivery_city_ref');
                    $currentDelivery->city_delivery_id = request()->get('delivery_city_id');
                    $currentDelivery->warehouse_title = request()->get('delivery_warehouse_title');
                    $currentDelivery->warehouse_ref = request()->get('delivery_warehouse_ref');
                }
                $currentDelivery->save();
            })
            ->setDisplayField('info.title')
            ->setDependedComponent(new DependedComponent('',$dependedCollection))
            ->displayRow()->addClass('pb-5');

    }
}
