<?php

namespace VmdCms\Modules\Users\Middleware;

use Closure;
use VmdCms\Modules\Users\Services\UserRouter;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->guard('user')->user();

        if($user && $user->active && !$user->deleted){
            return $next($request);
        }

        if($user){
            auth()->guard('user')->logout();
        }
        return redirect(route(UserRouter::ROUTE_LOGIN, [],false));
    }
}
