@extends('content::layouts.app')
@push('header_injection')
    <link rel="stylesheet" href="/assets/css/page-user-info.css">
@endpush

@section('content')

    <section class="page-user-info-section">
        <div class="container" data-cabinet-wrapper>
            <div class="left">
                @foreach($data->cabinetNavs as $item)
                <a href="{{$item->link}}" class="item @if($item->link == '#') isActive @endif">{{$item->title}}</a>
                @endforeach
                <a href="/logout" class="item">{{$data->getTranslate('cabinet_logout')}}</a>
            </div>
            <div class="right" data-cabinet-content-wrapper>
                @if($path = $data->contentViewPath ?? null)
                    @include($path)
                @endif
            </div>
        </div>
    </section>

@endsection

