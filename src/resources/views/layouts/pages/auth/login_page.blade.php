@extends('content::layouts.app',['wrapperClass' => 'page-login'])
@push('header_injection')
    <link rel="stylesheet" href="/assets/css/page-login.css">
@endpush

@section('content')

    @include('content::layouts.components.auth.login_section')

@endsection
@push('footer_injection')
{{--    <script src="/assets/js/validationJS.js" defer> </script>--}}

    <script defer>

        const recoveryPwd = document.getElementById('recoveryPwd');
        const popupRecoveryPwd = document.getElementsByClassName('popup-recoveryPwd')[0];
        const closedPopup = ['backgroundPopup', 'icoClosed', 'icoClosedTwo'];
        const recoveryPwdStepTwo = document.getElementById('recoveryPwdStepTwo');
        const versionOne = document.getElementById('versionOne');
        const versionTwo = document.getElementById('versionTwo');

        recoveryPwd.addEventListener('click', function () {

            popupRecoveryPwd.classList.add('active');
            versionTwo.classList.remove('active');
            versionOne.classList.add('active');

        })

        for (let pp = 0; pp < closedPopup.length; pp++){

            document.getElementsByClassName(closedPopup[pp])[0].addEventListener('click', function () {

                popupRecoveryPwd.classList.remove('active');

            })

        }

        recoveryPwdStepTwo.addEventListener('click', function () {

            versionOne.classList.remove('active');
            versionTwo.classList.add('active');

        })
    </script>
@endpush
