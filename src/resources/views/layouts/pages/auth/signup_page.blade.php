@extends('content::layouts.app',['wrapperClass' => 'page-login'])
@push('header_injection')
    <link rel="stylesheet" href="/assets/css/page-sign-up.css">
@endpush

@section('content')

    @include('content::layouts.components.auth.signup_section')

@endsection
@push('footer_injection')

    <script defer>

        const recoveryPwd = document.getElementById('buttonValidate');
        const popupRecoveryPwd = document.getElementsByClassName('popup-recoveryPwd')[0];
        const closedPopup = ['backgroundPopup', 'icoClosed'];
        const recoveryPwdStepTwo = document.getElementById('recoveryPwdStepTwo');
        const versionOne = document.getElementById('versionOne');
        const versionTwo = document.getElementById('versionTwo');

        recoveryPwd.addEventListener('click', function () {

            popupRecoveryPwd.classList.add('active');

        })

        for (let pp = 0; pp < closedPopup.length; pp++){

            console.log(closedPopup[pp]);

            document.getElementsByClassName(closedPopup[pp])[0].addEventListener('click', function () {

                popupRecoveryPwd.classList.remove('active');

            })

        }
    </script>
@endpush
