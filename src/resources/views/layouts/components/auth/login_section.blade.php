<div class="container">

    <section>

        <div class="left">

            <div class="left-contain">

                <div class="title">{{$data->getTranslate('already_registered')}}</div>

                <form method="POST" action="{{route(\VmdCms\Modules\Users\Services\UserRouter::ROUTE_LOGIN_POST,[],false)}}">
                    @csrf
                    <div @if($errors->has('email')) class="invalid" @endif>
                        <input type="email" name="email" placeholder="{{$data->getTranslate('email')}}" required>
                        <p class="form-error-input invalid">{{ $errors->first('email') }}</p>
                    </div>
                    <div @if($errors->has('password')) class="invalid" @endif>
                        <input type="password" name="password" placeholder="{{$data->getTranslate('password')}}">
                        <p class="form-error-input">{{ $errors->first('password') }}</p>
                    </div>
                    <button type="submit" class="button button-design type-one" id="authIDBottom" value=""> {{$data->getTranslate('login')}} </button>

                </form>
                <a href="javascript:void(0)" id="recoveryPwd">{{$data->getTranslate('forgot_password')}}</a>

                @include('content::layouts.components.auth.recover_password')

                <div class="sub-title">{{$data->getTranslate('social_login')}}</div>

                <div class="social">

                    <div class="item">

                        <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.94364 3.04602H8V0H5.58269V0.0109852C2.6537 0.115009 2.0534 1.76599 2.00049 3.50001H1.99447V5.02104H0V8.00401H1.99447V16H5.00025V8.00401H7.46247L7.93811 5.02104H5.00123V4.10207C5.00123 3.51605 5.39011 3.04602 5.94364 3.04602Z" fill="#3B5998"/>
                        </svg>

                        Facebook

                    </div>

                    <div class="item">

                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3.75 8.00012C3.75 7.2074 3.96841 6.4648 4.34784 5.82896V3.13184H1.65072C0.58025 4.52209 0 6.21643 0 8.00012C0 9.7838 0.58025 11.4781 1.65072 12.8684H4.34784V10.1713C3.96841 9.53543 3.75 8.79284 3.75 8.00012Z" fill="#FBBD00"/>
                            <path d="M8 12.2493L6.125 14.1243L8 15.9993C9.78372 15.9993 11.478 15.419 12.8683 14.3485V11.6543H10.174C9.53262 12.0351 8.78691 12.2493 8 12.2493Z" fill="#0F9D58"/>
                            <path d="M4.34947 10.1719L1.65234 12.869C1.86428 13.1443 2.09506 13.4078 2.34478 13.6576C3.85578 15.1686 5.86475 16.0007 8.00163 16.0007V12.2507C6.45088 12.2507 5.09172 11.4157 4.34947 10.1719Z" fill="#31AA52"/>
                            <path d="M16 7.99964C16 7.51292 15.9559 7.0253 15.869 6.55036L15.7987 6.16602H8V9.91601H11.7954C11.4268 10.6492 10.8601 11.2473 10.174 11.6547L12.8682 14.349C13.1435 14.137 13.4071 13.9062 13.6568 13.6565C15.1678 12.1455 16 10.1365 16 7.99964Z" fill="#3C79E6"/>
                            <path d="M11.0052 4.99478L11.3367 5.32622L13.9883 2.67459L13.6569 2.34316C12.1459 0.832156 10.1369 0 8 0L6.125 1.875L8 3.75C9.13519 3.75 10.2025 4.19206 11.0052 4.99478Z" fill="#CF2D48"/>
                            <path d="M7.9997 3.75V0C5.86283 0 3.85386 0.832156 2.34283 2.34313C2.09311 2.59284 1.86233 2.85644 1.65039 3.13172L4.34752 5.82884C5.0898 4.585 6.44895 3.75 7.9997 3.75Z" fill="#EB4132"/>
                        </svg>

                        Google

                    </div>

                </div>

            </div>

        </div>

        <div class="right">
            @if($block = $data->getBlock('login_info'))

                <div class="contain-right">

                    <div class="block-top">

                        <div class="title">{{$block->title}}</div>

                        <a href="{{$block->link}}" class="button-design type-three">{{$block->link_title}}</a>

                    </div>

                    <div class="block-button">

                        <div class="right-sub-title">{{$block->benefits_title}}</div>

                        @if($block->children && count($block->children))
                            @foreach($block->children as $child)
                                <div class="withIcon">
                                    <img src="{{$child->mediaPath}}" alt="{{$child->title}}">
                                    <span>{{$child->title}}</span>

                                </div>
                            @endforeach
                        @endif

                    </div>

                </div>

            @endif
        </div>

    </section>

</div>
