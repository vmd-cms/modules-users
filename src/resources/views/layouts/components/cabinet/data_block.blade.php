<form data-form-user-data method="POST" action="{{route(App\Modules\Users\Services\UserRouter::ROUTE_CABINET_UPDATE_USER_DATA,[],false)}}">
    @csrf

    @if(!$data->user->phoneApproved)
        <div class="alert">

            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                    <path d="M23.7709 20.082L13.4632 2.22857C13.1578 1.69959 12.6108 1.38379 12 1.38379C11.3892 1.38379 10.8422 1.69959 10.5368 2.22857L0.229078 20.082C-0.0763594 20.611 -0.0763594 21.2427 0.229078 21.7716C0.534469 22.3006 1.08145 22.6164 1.69228 22.6164H22.3077C22.9185 22.6164 23.4655 22.3006 23.7709 21.7716C24.0764 21.2427 24.0764 20.611 23.7709 20.082ZM22.5516 21.0676C22.5213 21.1202 22.4491 21.2084 22.3078 21.2084H1.69228C1.55081 21.2084 1.47872 21.1203 1.44844 21.0676C1.41811 21.0151 1.3777 20.9085 1.44844 20.7861L11.7561 2.93254C11.8268 2.81005 11.9392 2.79173 12 2.79173C12.0607 2.79173 12.1731 2.81001 12.2438 2.93254L22.5516 20.7861C22.6223 20.9085 22.5819 21.0151 22.5516 21.0676Z" fill="#FC7A03"/>
                    <path d="M12.7049 7.78516H11.2969V15.2944H12.7049V7.78516Z" fill="#FC7A03"/>
                    <path d="M12.0012 18.5805C12.5196 18.5805 12.9398 18.1602 12.9398 17.6418C12.9398 17.1234 12.5196 16.7031 12.0012 16.7031C11.4828 16.7031 11.0625 17.1234 11.0625 17.6418C11.0625 18.1602 11.4828 18.5805 12.0012 18.5805Z" fill="#FC7A03"/>
                </g>
                <defs>
                    <clipPath id="clip0">
                        <rect width="24" height="24" fill="white"/>
                    </clipPath>
                </defs>
            </svg>

            {{$data->getTranslate('approve_phone_text')}}

        </div>
    @endif

    <div class="right_left">

        <div class="right-left-title">{{$data->getTranslate('cabinet_data')}}</div>

        @if($data->user->phoneApproved)
            <div class="right-left_sub-title">{{$data->getTranslate('personal_discount')}} - <span>{{$data->user->discountPercent}}%</span></div>
        @endif

        <div @if($errors->has('first_name')) class="invalid" @endif>
            <input class="input" type="text" name="first_name" placeholder="{{$data->getTranslate('your_name')}}" value="{{$data->user->firstName}}" required>
            <p class="form-error-input">{{$errors->first('first_name')}}</p>
        </div>

        <div @if($errors->has('last_name')) class="invalid" @endif>
            <input class="input" type="text" name="last_name" placeholder="{{$data->getTranslate('your_last_name')}}" value="{{$data->user->lastName}}" required>
            <p class="form-error-input">{{$errors->first('last_name')}}</p>
        </div>

        <div class="input-info">

            <input class="input" data-updated-phone type="hidden" name="updated_phone" value="">
            <div @if($errors->has('updated_phone')) class="invalid" @endif>
                <input class="input" data-phone-disabled type="tel" placeholder="{{$data->getTranslate('your_phone')}}" value="{{$data->user->phone}}" required disabled>

                @if(!$data->user->phoneApproved)
                <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M6.9987 10.8411C7.35826 10.8411 7.64974 10.5497 7.64974 10.1901C7.64974 9.83054 7.35826 9.53906 6.9987 9.53906C6.63914 9.53906 6.34766 9.83054 6.34766 10.1901C6.34766 10.5497 6.63914 10.8411 6.9987 10.8411Z"
                        fill="black"/>
                    <path
                        d="M6.9987 0.333008C3.31422 0.333008 0.332031 3.3147 0.332031 6.99967C0.332031 10.6842 3.31372 13.6663 6.9987 13.6663C10.6832 13.6663 13.6654 10.6846 13.6654 6.99967C13.6654 3.3152 10.6837 0.333008 6.9987 0.333008ZM6.9987 12.6247C3.88992 12.6247 1.3737 10.1089 1.3737 6.99967C1.3737 3.8909 3.88951 1.37467 6.9987 1.37467C10.1075 1.37467 12.6237 3.89048 12.6237 6.99967C12.6237 10.1085 10.1079 12.6247 6.9987 12.6247Z"
                        fill="black"/>
                    <path
                        d="M6.9974 3.67969C5.84865 3.67969 4.91406 4.61427 4.91406 5.76302C4.91406 6.05068 5.14724 6.28385 5.4349 6.28385C5.72255 6.28385 5.95573 6.05068 5.95573 5.76302C5.95573 5.18865 6.42302 4.72135 6.9974 4.72135C7.57177 4.72135 8.03906 5.18865 8.03906 5.76302C8.03906 6.3374 7.57177 6.80469 6.9974 6.80469C6.70974 6.80469 6.47656 7.03786 6.47656 7.32552V8.6276C6.47656 8.91526 6.70974 9.14844 6.9974 9.14844C7.28505 9.14844 7.51823 8.91526 7.51823 8.6276V7.78044C8.41578 7.54859 9.08073 6.73201 9.08073 5.76302C9.08073 4.61427 8.14615 3.67969 6.9974 3.67969Z"
                        fill="black"/>
                </svg>

                <div class="hidde-info">{{$data->getTranslate('approve_phone_text')}}</div>
                @endif
                <p class="form-error-input">{{$errors->first('updated_phone')}}</p>
            </div>

        </div>
        @if($data->user->phoneApproved)
            <a class="edit-phone-number" data-open-change-phone-modal>{{$data->getTranslate('change_phone')}}</a>
            @include('content::layouts.components.cabinet.change_phone_modal')
        @else
            <input type="button" value="{{$data->getTranslate('confirm_phone')}}" class="applyEditNumber">
            @include('content::layouts.components.cabinet.confirm_phone_modal')
        @endif

        <div @if($errors->has('email')) class="invalid" @endif>
            <input class="input" type="email" name="email" placeholder="{{$data->getTranslate('email')}}" required>
            <p class="form-error-input">{{$errors->first('email')}}</p>
        </div>

        <div class="right-left-title title-edit-pwd">{{$data->getTranslate('change_password')}}</div>

        <div @if($errors->has('new_password')) class="invalid" @endif>
            <input type="password" class="input" name="new_password" placeholder="{{$data->getTranslate('new_password')}}">
            <p class="form-error-input">{{$errors->first('new_password')}}</p>
        </div>

    </div>

    <div class="right_right">

        <div class="right-right-title">{{$data->getTranslate('delivery_data')}}</div>

        <div class="custom-contain-of-option">

            <div id="custom_option_0" class="custom_option_sectionOfPageCart">

                                                <span>

                                                    <div>Новая почта</div>

                                                </span>

                <div class="container_option">

                    <div class="option" key="1-1">Новая почта</div>
                    <div class="option" key="1-2">92 таблеток</div>
                    <div class="option" key="1-3">93 таблеток</div>

                </div>

                <svg class="rowOfOption" width="10" height="5" viewBox="0 0 10 5" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.5"
                          d="M5.27441 4.89451L9.88692 0.612382C10.038 0.472083 10.0377 0.244936 9.88614 0.104872C9.73459 -0.0350833 9.48911 -0.0347217 9.33795 0.105595L4.99998 4.13284L0.662029 0.10545C0.510858 -0.0348487 0.265527 -0.0352104 0.113965 0.104727C0.0379887 0.17494 -1.16676e-08 0.266923 -1.56883e-08 0.358907C-1.96988e-08 0.450655 0.037735 0.542277 0.113184 0.612363L4.72557 4.89451C4.79819 4.96208 4.89701 5 4.99998 5C5.10295 5 5.20166 4.96197 5.27441 4.89451Z"
                          fill="black"></path>
                </svg>

            </div>
            <div id="custom_option_1" class="custom_option_sectionOfPageCart">

                                                <span>

                                                    <div>Одесса</div>

                                                </span>

                <div class="container_option">

                    <div class="option" key="1-1">Одесса</div>
                    <div class="option" key="1-2">92 таблеток</div>
                    <div class="option" key="1-3">93 таблеток</div>

                </div>

                <svg class="rowOfOption" width="10" height="5" viewBox="0 0 10 5" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.5"
                          d="M5.27441 4.89451L9.88692 0.612382C10.038 0.472083 10.0377 0.244936 9.88614 0.104872C9.73459 -0.0350833 9.48911 -0.0347217 9.33795 0.105595L4.99998 4.13284L0.662029 0.10545C0.510858 -0.0348487 0.265527 -0.0352104 0.113965 0.104727C0.0379887 0.17494 -1.16676e-08 0.266923 -1.56883e-08 0.358907C-1.96988e-08 0.450655 0.037735 0.542277 0.113184 0.612363L4.72557 4.89451C4.79819 4.96208 4.89701 5 4.99998 5C5.10295 5 5.20166 4.96197 5.27441 4.89451Z"
                          fill="black"></path>
                </svg>

            </div>
            <div id="custom_option_2" class="custom_option_sectionOfPageCart">

                                                <span>

                                                    <div>Отделение №3: Дальницкая, 23</div>

                                                </span>

                <div class="container_option">

                    <div class="option" key="1-1">Отделение №3: Дальницкая, 23</div>
                    <div class="option" key="1-2">92 таблеток</div>
                    <div class="option" key="1-3">93 таблеток</div>

                </div>

                <svg class="rowOfOption" width="10" height="5" viewBox="0 0 10 5" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.5"
                          d="M5.27441 4.89451L9.88692 0.612382C10.038 0.472083 10.0377 0.244936 9.88614 0.104872C9.73459 -0.0350833 9.48911 -0.0347217 9.33795 0.105595L4.99998 4.13284L0.662029 0.10545C0.510858 -0.0348487 0.265527 -0.0352104 0.113965 0.104727C0.0379887 0.17494 -1.16676e-08 0.266923 -1.56883e-08 0.358907C-1.96988e-08 0.450655 0.037735 0.542277 0.113184 0.612363L4.72557 4.89451C4.79819 4.96208 4.89701 5 4.99998 5C5.10295 5 5.20166 4.96197 5.27441 4.89451Z"
                          fill="black"></path>
                </svg>

            </div>

        </div>

    </div>

    <div class="down-block">

        <button class="button-design type-three" type="button" onclick="document.location.href = '/'">{{$data->getTranslate('cancel')}}</button>
        <button class="button-design type-one" type="submit">{{$data->getTranslate('save')}}</button>

    </div>

</form>
@push('footer_injection')
    <script>
        document.querySelector('main').classList.add('page-user-info');
    </script>
@endpush
