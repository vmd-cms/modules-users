<div class="view-component-container">
    @if(isset($model))

        @php
            $orderEntity = class_exists(\VmdCms\Modules\Orders\Entity\OrderEntity::class) ? new \VmdCms\Modules\Orders\Entity\OrderEntity() : null;
            $countOrders = $orderEntity ? $orderEntity->getUserTotalOrdersCount($model) : null;
            $countOrdersByStatus = $orderEntity ? $orderEntity->getUserOrdersCountByStatus($model) : null;
        @endphp
        <div class="user-profile-preview pa-10">
            <div class="head-block">
                <div class="photo-block">
                    <img src="{{$model->imagePath}}">
                </div>
                <div class="info-block">
                    <div class="text-title">
                        <span>{{$model->fullName}}</span>
                    </div>
                    @if(!is_null($countOrders))
                    <div class="group text-info pt-1">
                        <span class="icon icon-shopping-cart"></span>
                        <span> {{$countOrders}}</span>
                        <span> заказов</span>
                    </div>
                    @endif
                </div>
            </div>
            <div class="body-block">
                <div class="text-info pt-2">
                    <span>Пользователь зарегистрирован</span>
                </div>

                @if(is_countable($countOrdersByStatus))
                <div class="group text-info pt-2">
                    <span>Заказы:</span>
                    @foreach($countOrdersByStatus as $orderStatus=>$count)
                        <span>{{$orderStatus}}&nbsp-&nbsp</span>
                        <span class="text-title">{{$count}}</span>
                        <span>;&nbsp</span>
                    @endforeach
                </div>
                @endif

                @if($model->userGroup)
                    <div class="group pt-2">
                        <span class="text-info">Группа клиента:</span>
                        <span class="text-title">{{$model->userGroup->info->title ?? null}}</span>
                    </div>
                @endif
                <div class="group pt-2">
                    <span class="text-info">Город:</span>
                    <span class="text-title">{{$model->city}}</span>
                </div>
                <div class="group pt-2">
                    <span class="icon icon-message"></span>
                    <span class="text-title pl-2">{{$model->email}}</span>
                </div>
                <div class="group pt-2">
                    <span class="icon icon-phone"></span>
                    <span class="text-title pl-2">{{$model->phone}}</span>
                </div>
            </div>
        </div>
    @endif
</div>
<style>
    .user-profile-preview .head-block{
        display: flex;
        justify-content: flex-start;
        align-items: center;
    }
    .user-profile-preview .head-block .photo-block{
        width: 46px;
        height: 46px;
        display: flex;
        align-items: center;
    }
    .user-profile-preview .head-block .photo-block img{
        max-width: 100%;
        object-fit: contain;
        border-radius: 50%;
    }
    .user-profile-preview .head-block .info-block{
        padding-left: 15px;
    }
</style>
