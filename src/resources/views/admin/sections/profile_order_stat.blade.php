<div class="view-component-container">
    @if(isset($model) && class_exists(\VmdCms\Modules\Orders\Entity\OrderEntity::class))

        @php
            $orderEntity = new \VmdCms\Modules\Orders\Entity\OrderEntity();
            $sumOrders = $orderEntity->getUserTotalOrdersSum($model);
            $countOrders = $orderEntity->getUserTotalOrdersCount($model);
            $countPayedOrders = $orderEntity->getUserOrdersCountByPayment($model);
            $countNotPayedOrders = $orderEntity->getUserOrdersCountByPayment($model,false);
        @endphp

        <div class="user-profile-preview pa-10">
            <div>
                <span class="stat-title">Статистика</span>
            </div>

            <div class="group-stat pt-3">
                <span class="text-info">Сумма заказов:</span>
                <span class="text-title">{{$sumOrders}}</span>
            </div>

            <div class="group-stat pt-3">
                <span class="text-info">Количество заказов:</span>
                <span class="text-title">{{$countOrders}}</span>
            </div>

            <div class="group-stat pt-3">
                <span class="text-info">Кол-во оплаченных заказов:</span>
                <span class="text-title">{{$countPayedOrders}}</span>
            </div>

            <div class="group-stat pt-3">
                <span class="text-info">Кол-во неоплаченных заказов:</span>
                <span class="text-title">{{$countNotPayedOrders}}</span>
            </div>
        </div>
    @endif
</div>
<style>
    .stat-title{
        font-size: 16px;
        line-height: 19px;
        color: #192A3E;
        font-weight: 600;
    }
    .group-stat{
        display: flex;
        justify-content: space-between;
    }
</style>
