<?php

return [
    /**
     * fields
     **/
    'registered' => 'Зарегистрирован',
    'create_customer' => 'Создание нового клиента',
    'approved' => 'Подтвержден',
    'personal_discount' => 'Персональная скидка',
    'city' => 'Город',
    'comments' => 'Коментарии',
    'ip_address' => 'Ip-адрес',
    'change_password' => 'Изменить пароль',
    'restore' => 'Восстановить',
    'delivery' => 'Доставка',
    'benefits' => 'Преимущества',

    /**
     * pages
     */
    'cabinet' => 'Кабинет',
    'auth' => 'Авторизация',
    'users' => 'Пользователи',
    'customers' => 'Клиенты',
    'customers_groups' => 'Группы клиентов',
    'subscribers' => 'Подписчики',
    'cabinet_menu' => 'Меню Кабинет',
    'info_blocks_cabinet' => 'Инфоблоки Кабинет',
    'info_blocks_auth' => 'Инфоблоки Авторизация',
    'translates_cabinet' => 'Переводы Кабинет',
    'translates_auth' => 'Переводы Авторизация',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
