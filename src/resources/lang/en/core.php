<?php

return [
    /**
     * fields
     **/
    'registered' => 'Registered',
    'create_customer' => 'Create new customer',
    'approved' => 'Approved',
    'personal_discount' => 'Personal Discount',
    'city' => 'City',
    'comments' => 'Comments',
    'ip_address' => 'Ip-address',
    'change_password' => 'Change password',
    'restore' => 'Restore',
    'delivery' => 'Delivery',
    'benefits' => 'Benefits',

    /**
     * pages
     */
    'cabinet' => 'Cabinet',
    'auth' => 'Auth',
    'users' => 'Users',
    'customers' => 'Customers',
    'customers_groups' => 'Customers Groups',
    'subscribers' => 'Subscribers',
    'cabinet_menu' => 'Cabinet Menu',
    'info_blocks_cabinet' => 'Info Blocks Cabinet',
    'info_blocks_auth' => 'Info Blocks Auth',
    'translates_cabinet' => 'Translates Cabinet',
    'translates_auth' => 'Translates Auth',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
