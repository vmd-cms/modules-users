<?php

namespace VmdCms\Modules\Users\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Users\Entity\Auth\ApiAuthEntity;
use VmdCms\Modules\Users\Entity\Auth\AuthEntity;
use App\Modules\Content\Entity\Translates;
use App\Modules\Users\Models\Components\AuthTranslate;
use App\Modules\Users\DTO\UserDTO;

class RecoveryController extends CoreController
{

    public function recoveryPassword(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            return ApiResponse::error(['errors' => $validator->errors()->toArray()]);
        }

        try {
            (new AuthEntity())->recoveryPassword($request->get('email'));
        }catch (\Exception $exception){
            return ApiResponse::error(['error' => $exception->getMessage()]);
        }

        Translates::getInstance()->appendGroups([AuthTranslate::getModelGroup()]);
        $message = Translates::getInstance()->getValue('sent_recovery_pass_email');

        return ApiResponse::success(['success' => $message]);
    }

    public function oauthRecoveryToken($token)
    {
        $tokenStr = filter_var(urldecode($token),FILTER_SANITIZE_STRING);

        try {
            (new AuthEntity())->oauthRecoveryToken($tokenStr);
        }catch (\Exception $exception){
            return ApiResponse::error(['error_code' => 'Token not found'], 404);
        }
        $user = AuthEntity::getAuthUser();
        $session = ApiAuthEntity::createSession($user);
        return ApiResponse::success([
            'token' => $session->token,
            'user' => (new UserDTO($user))->toArray()
        ]);
    }
}
