<?php

namespace VmdCms\Modules\Users\Controllers\Api;

use App\Modules\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Users\DTO\UserDTO;
use VmdCms\Modules\Users\Entity\Auth\ApiAuthEntity;
use VmdCms\Modules\Users\Services\CoreEventEnums;

class SignupController extends CoreController
{
    public function signup(Request $request)
    {
        $validator = Validator::make($request->toArray(),
            [
                'first_name' => 'required|string|max:64',
                'last_name' => 'string|max:64',
                'phone' => 'required|unique:users,phone',
                'email' => 'required|unique:users,email',
                'password' => 'min:6|max:64'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::error(['errors' => $validator->errors()->toArray()]);
        }

        try {
            $user = new User();
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->password = Hash::make($request->get('password'));
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->active = true;
            $user->save();

            Event::dispatch(CoreEventEnums::USER_SIGN_UP,['user_id' => $user->id]);

            $session = ApiAuthEntity::createSession($user);
            ApiAuthEntity::authUser($user);

            Event::dispatch(CoreEventEnums::USER_SIGN_IN,['is_auto_sign_in' => true]);

        }catch (\Exception $exception){

            Event::dispatch(CoreEventEnums::USER_SIGN_UP_FAIL,[
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ]);
            return ApiResponse::error([
                'error_code' => 'user_signup_error'
            ]);
        }

        return ApiResponse::success([
            'token' => $session->token,
            'user' => (new UserDTO($user))->toArray()
        ]);
    }

}
