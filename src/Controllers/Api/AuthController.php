<?php

namespace VmdCms\Modules\Users\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\Services\Auth\ThrottlesLogins;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Users\Entity\Auth\ApiAuthEntity;
use VmdCms\Modules\Users\Services\CoreEventEnums;

class AuthController extends CoreController
{
    use ThrottlesLogins;
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api_user');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return ApiResponse::error("Auth locked");
        }

        if ($this->attemptLogin($request)) {
            $this->signInEvent();
            return ApiResponse::success(['token' => $this->getApiToken()]);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);


        return ApiResponse::error("Auth failed");
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    protected function signInEvent(){
        Event::dispatch(CoreEventEnums::USER_SIGN_IN);
    }

    protected function getApiToken(){
        $session = ApiAuthEntity::createSession($this->guard()->user());
        return $session->token;
    }

    public function logout(Request $request){
        try {
            $user = ApiAuthEntity::getUserByToken($request->header('token'));
            ApiAuthEntity::suspendSessions($user);
            Event::dispatch(CoreEventEnums::USER_LOGOUT,['user_id' => $user->id]);
        }catch (\Exception $exception){
            return ApiResponse::error('Wrong sessions');
        }

        return ApiResponse::success(['status'=>true]);
    }

}
