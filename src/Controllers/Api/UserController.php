<?php

namespace VmdCms\Modules\Users\Controllers\Api;

use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Users\DTO\UserDTO;
use VmdCms\Modules\Users\Entity\Auth\ApiAuthEntity;
use VmdCms\Modules\Users\Models\User;

class UserController extends CoreController
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieveAuthUser(Request $request){
        $user = ApiAuthEntity::getAuthUser();
        if(!$user instanceof User){
            return ApiResponse::error(['error_code' => 'user_not_auth']);
        }
        return ApiResponse::success(['user' => (new UserDTO($user))->toArray()]);
    }

}
