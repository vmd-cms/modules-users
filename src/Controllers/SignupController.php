<?php

namespace VmdCms\Modules\Users\Controllers;

use App\Modules\Content\DTO\BlockDTO;
use App\Modules\Content\Services\DataShare;
use App\Modules\Translates\Models\Groups\FormGroupTranslate;
use App\Modules\Users\Models\Components\AuthTranslate;
use App\Modules\Users\Models\Components\Blocks\SignUpInfoBlock;
use App\Modules\Users\Models\User;
use App\Modules\Users\Services\UserRouter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use VmdCms\CoreCms\Controllers\CoreController;

class SignupController extends CoreController
{
    public function __construct()
    {
        DataShare::getInstance()->translates->appendGroups([FormGroupTranslate::getModelGroup(),AuthTranslate::getModelGroup()]);
    }

    public function signupPage()
    {
        DataShare::getInstance()->blocks->append(new BlockDTO(SignUpInfoBlock::getModelTree()) );
        return view('content::layouts.pages.auth.signup_page');
    }

    public function signupEmail(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:64',
            'last_name' => 'string|max:64',
            'phone' => 'required|unique:users,phone',
            'email' => 'required|unique:users,email',
            'password' => 'min:6|max:64',
            'confirm_terms' => 'required'
        ]);

        $user = new User();
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->password = Hash::make($request->get('password'));
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->save();

        auth()->guard('user')->login($user,true);

        return redirect(\route(UserRouter::ROUTE_CABINET,[],false));
    }

}
