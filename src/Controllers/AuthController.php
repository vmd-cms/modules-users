<?php

namespace VmdCms\Modules\Users\Controllers;

use App\Modules\Content\DTO\BlockDTO;
use App\Modules\Users\Models\Components\AuthTranslate;
use App\Modules\Users\Models\Components\Blocks\LogInInfoBlock;
use App\Modules\Content\Services\DataShare;
use App\Modules\Translates\Models\Groups\FormGroupTranslate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\Services\Auth\AuthenticatesUsers;
use VmdCms\CoreCms\Services\BreadCrumbs;
use VmdCms\Modules\Users\Services\UserRouter;

class AuthController extends CoreController
{
    public function __construct()
    {
        DataShare::getInstance()->translates->appendGroups([FormGroupTranslate::getModelGroup(),AuthTranslate::getModelGroup()]);
    }

    use AuthenticatesUsers;

    protected $redirectTo = '/cabinet';

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }

    protected function shareBreadCrumbs(string $action = 'login')
    {
        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto(' ',DataShare::getInstance()->getTranslate($action)));
        DataShare::getInstance()->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if($this->guard()->check()) {
            return redirect(route(UserRouter::ROUTE_CABINET));
        }
        if($block = LogInInfoBlock::getModelTree()){
            DataShare::getInstance()->blocks->append(new BlockDTO($block));
        }
        $this->shareBreadCrumbs('login');
        return view('content::layouts.pages.auth.login_page');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect('/');
    }

}
