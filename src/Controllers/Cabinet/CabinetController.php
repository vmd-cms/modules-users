<?php

namespace VmdCms\Modules\Users\Controllers\Cabinet;

use App\Modules\Users\Entity\Cabinet\CabinetEntity;
use App\Modules\Users\Services\UserRouter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Controllers\CoreController;
use VmdCms\Modules\Users\Models\User;

class CabinetController extends CoreController
{
    public function cabinetPage(Request $request)
    {
        $route = Route::getCurrentRoute();
        $cabinetEntity = new CabinetEntity();
        $cabinetEntity->sharePageData($route);
        return view('content::layouts.pages.cabinet.cabinet_page')->render();
    }

    public function updateUserData(Request $request)
    {
        $user = auth()->guard('user')->user();
        if(!$user instanceof User) abort(403);

        $request->validate([
            'first_name' => 'required|string|max:64',
            'last_name' => 'nullable|string|max:64',
            'updated_phone' => 'nullable|unique:users,phone,' . $user->id,
            'email' => 'required|unique:users,email,' . $user->id,
            'new_password' => 'nullable|min:6|max:64',
        ]);

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        if($phone = $request->get('updated_phone'))
        {
            if($user->phone !== $phone)
            {
                $user->phone = $phone;
                //todo unverify user
            }
        }
        if($password = $request->get('new_password'))
        {
            $user->password = Hash::make($password);
        }
        $user->save();
        return redirect(\route(UserRouter::ROUTE_CABINET,[],false));
    }
}
