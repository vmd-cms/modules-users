<?php

namespace VmdCms\Modules\Users\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Users\Models\User;

interface UserDTOInterface extends Arrayable
{
    public function __construct(User $user);
}
