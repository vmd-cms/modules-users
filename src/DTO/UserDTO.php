<?php

namespace VmdCms\Modules\Users\DTO;

use VmdCms\Modules\Users\Contracts\UserDTOInterface;
use VmdCms\Modules\Users\Models\User;

class UserDTO implements UserDTOInterface
{
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $phone;
    protected $discountPercent;
    protected $confirmed;

    /**
     * UserDTO constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->firstName = $user->first_name;
        $this->lastName = $user->last_name;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->discountPercent = $user->discount_percent;
        $this->confirmed = $user->confirmed;
    }


    /**
     * @return null|string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return float
     */
    public function getDiscountPercent(): float
    {
        return $this->discountPercent;
    }

    /**
     * @return bool
     */
    public function getConfirmed(): bool
    {
        return $this->confirmed;
    }

    public function toArray()
    {
        return [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'email' => $this->email,
            'phone' => $this->phone,
            'discountPercent' => $this->discountPercent,
            'confirmed' => $this->confirmed,
        ];
    }
}
