<?php

namespace VmdCms\Modules\Users\DTO;

use VmdCms\Modules\Orders\DTO\OrderParamDTO;
use VmdCms\Modules\Users\Models\UserDelivery;
use Illuminate\Contracts\Support\Arrayable;

class UserDeliveryDTO implements Arrayable
{
    protected $orderParamId;
    protected $cityTitle;
    protected $cityRef;
    protected $cityDeliveryId;
    protected $warehouseTitle;
    protected $warehouseRef;

    /**
     * @var OrderParamDTO
     */
    protected $delivery;

    /**
     * UserDTO constructor.
     * @param UserDelivery $userDelivery
     */
    public function __construct(UserDelivery $userDelivery)
    {
        $this->orderParamId = $userDelivery->order_param_id;
        $this->cityTitle = $userDelivery->city_title;
        $this->cityRef = $userDelivery->city_ref;
        $this->cityDeliveryId = $userDelivery->city_delivery_id;
        $this->warehouseTitle = $userDelivery->warehouse_title;
        $this->warehouseRef = $userDelivery->warehouse_ref;
        $this->delivery = new OrderParamDTO($userDelivery->delivery);
    }

    /**
     * @return OrderParamDTO
     */
    public function getDelivery(): OrderParamDTO
    {
        return $this->delivery;
    }

    /**
     * @return mixed
     */
    public function getOrderParamId()
    {
        return $this->orderParamId;
    }

    /**
     * @return mixed
     */
    public function getCityTitle()
    {
        return $this->cityTitle;
    }

    /**
     * @return mixed
     */
    public function getCityRef()
    {
        return $this->cityRef;
    }

    /**
     * @return mixed
     */
    public function getCityDeliveryId()
    {
        return $this->cityDeliveryId;
    }

    /**
     * @return mixed
     */
    public function getWarehouseTitle()
    {
        return $this->warehouseTitle;
    }

    /**
     * @return mixed
     */
    public function getWarehouseRef()
    {
        return $this->warehouseRef;
    }



    public function toArray()
    {
        return [
            'orderParamId' => $this->orderParamId,
            'cityTitle' => $this->cityTitle,
            'cityRef' => $this->cityRef,
            'cityDeliveryId' => $this->cityDeliveryId,
            'warehouseTitle' => $this->warehouseTitle,
            'warehouseRef' => $this->warehouseRef,
        ];
    }
}
