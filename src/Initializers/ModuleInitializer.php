<?php

namespace VmdCms\Modules\Users\Initializers;;

use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventModelDTO;
use VmdCms\CoreCms\CoreModules\Events\Entity\CoreEventsSetup;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;
use VmdCms\Modules\Users\Services\CoreEventEnums;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'users';
    const ALIAS = 'Users';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishServices(true);
        $this->stubBuilder->setPublishDTO(true);
        $this->stubBuilder->setPublishEntity(true);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    public function seedCoreEvents()
    {
        CoreEventsSetup::getInstance()
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_SIGN_IN))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_SIGN_UP))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_SIGN_UP_FAIL))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_RECOVERY_PASSWORD_EMAIL))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_RECOVERY_PASSWORD_SMS))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_RECOVERY_PASSWORD_EMAIL_CONFIRMED))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_RECOVERY_PASSWORD_SMS_CONFIRMED))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->seed();
    }
}
