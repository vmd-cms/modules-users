<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use App\Modules\Users\Services\UserRouter;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\Users\\Controllers",
    ],function (Router $router){
        $router->get('login', [
            'as'   => UserRouter::ROUTE_LOGIN,
            'uses' => 'AuthController@showLoginForm',
        ]);
        $router->post('login', [
            'as'   => UserRouter::ROUTE_LOGIN_POST,
            'uses' => 'AuthController@login',
        ]);
        $router->any('logout', [
            'as'   => UserRouter::ROUTE_LOGOUT,
            'uses' => 'AuthController@logout',
        ]);
        $router->get('signup', [
            'as'   => UserRouter::ROUTE_SIGNUP,
            'uses' => 'SignupController@signupPage',
        ]);
        $router->post('signup', 'SignupController@signupEmail');
        Route::group([
            'middleware' => \VmdCms\Modules\Users\Middleware\UserAuth::class,
            'namespace' => "Cabinet",
            'prefix' => "cabinet",
        ],function (Router $router){
            $router->get('', [
                'as'   => UserRouter::ROUTE_CABINET,
                'uses' => 'CabinetController@cabinetPage',
            ]);
        });

    });
};
