<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use VmdCms\Modules\Users\Services\ApiUserRouter;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\Users\\Controllers\\Api",
    ],function (Router $router){

        $router->post('signin', [
            'as'   => ApiUserRouter::ROUTE_SIGN_IN,
            'uses' => 'AuthController@login',
        ])->withoutMiddleware([\VmdCms\CoreCms\Middleware\ApiAuthMiddleware::class]);
        $router->any('logout', [
            'as'   => ApiUserRouter::ROUTE_LOGOUT,
            'uses' => 'AuthController@logout',
        ])->withoutMiddleware([\VmdCms\CoreCms\Middleware\ApiAuthMiddleware::class]);
        $router->post('signup', [
            'as'   => ApiUserRouter::ROUTE_SIGN_UP,
            'uses' => 'SignupController@signup',
        ])->withoutMiddleware([\VmdCms\CoreCms\Middleware\ApiAuthMiddleware::class]);
        $router->post('password/recovery', [
            'as'   => ApiUserRouter::ROUTE_RECOVERY_PASSWORD,
            'uses' => 'RecoveryController@recoveryPassword',
        ])->withoutMiddleware([\VmdCms\CoreCms\Middleware\ApiAuthMiddleware::class]);
        $router->get('oauth/recovery/{token}', [
            'as'   => ApiUserRouter::ROUTE_OAUTH_RECOVERY_TOKEN,
            'uses' => 'RecoveryController@oauthRecoveryToken',
        ])->withoutMiddleware([\VmdCms\CoreCms\Middleware\ApiAuthMiddleware::class]);

        $router->get('user', [
            'as'   => ApiUserRouter::ROUTE_USER_GET,
            'uses' => 'UserController@retrieveAuthUser',
        ]);
    });
};
