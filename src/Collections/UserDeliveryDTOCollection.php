<?php

namespace VmdCms\Modules\Users\Collections;

use App\Modules\Users\DTO\UserDeliveryDTO;
use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class UserDeliveryDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param UserDeliveryDTO $userDeliveryDTO
     */
    public function appendItem(UserDeliveryDTO $userDeliveryDTO): void
    {
        $this->collection->put($userDeliveryDTO->getOrderParamId(), $userDeliveryDTO);
    }

    /**
     * @param $key
     * @return UserDeliveryDTO|null
     */
    public function getByKey($key): ?UserDeliveryDTO
    {
        return $this->collection->get($key);
    }
}
