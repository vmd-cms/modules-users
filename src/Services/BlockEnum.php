<?php

namespace VmdCms\Modules\Users\Services;

use VmdCms\CoreCms\Services\Enums;

class BlockEnum extends Enums
{
    const CABINET_INFO_BLOCK = 'cabinet_info';
    const LOGIN_INFO_BLOCK = 'login_info';
    const SIGNUP_INFO_BLOCK = 'signup_info';
}
