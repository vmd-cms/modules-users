<?php

namespace VmdCms\Modules\Users\Services;

class ApiUserRouter
{
    const API_USER_AUTH_PREFIX = 'user_auth.';

    const ROUTE_SIGN_UP = self::API_USER_AUTH_PREFIX . 'sign_up';
    const ROUTE_SIGN_IN = self::API_USER_AUTH_PREFIX . 'sign_in';
    const ROUTE_LOGOUT = self::API_USER_AUTH_PREFIX . 'logout';

    const ROUTE_USER_GET = self::API_USER_AUTH_PREFIX . 'user_get';

    const ROUTE_RECOVERY_PASSWORD = self::API_USER_AUTH_PREFIX . 'recovery-password';
    const ROUTE_OAUTH_RECOVERY_TOKEN = self::API_USER_AUTH_PREFIX . 'oauth-recovery-token';


}
