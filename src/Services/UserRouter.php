<?php

namespace VmdCms\Modules\Users\Services;

class UserRouter
{
    const USER_AUTH_PREFIX = 'userAuth.';

    const ROUTE_LOGIN = self::USER_AUTH_PREFIX . 'login';
    const ROUTE_LOGIN_POST = self::USER_AUTH_PREFIX . 'login_post';
    const ROUTE_LOGOUT = self::USER_AUTH_PREFIX . 'logout';
    const ROUTE_SIGNUP = self::USER_AUTH_PREFIX . 'signup';

    const USER_CABINET_PREFIX = 'userCabinet.';

    const ROUTE_CABINET = self::USER_CABINET_PREFIX . 'cabinet';
}
