<?php

namespace VmdCms\Modules\Users\Services;

use VmdCms\CoreCms\Services\BlockSectionFactoryAbstract;
use App\Modules\Users\Models\Components\Blocks\CabinetInfoBlock;
use App\Modules\Users\Models\Components\Blocks\LogInInfoBlock;
use App\Modules\Users\Models\Components\Blocks\SignUpInfoBlock;

class BlockSectionFactory extends BlockSectionFactoryAbstract
{
    protected static function getAssocKeySectionClasses() : array
    {
        return [
            CabinetInfoBlock::getModelKey() => \App\Modules\Users\Sections\Components\Blocks\CabinetInfoBlock::class,
            LogInInfoBlock::getModelKey() => \App\Modules\Users\Sections\Components\Blocks\LogInInfoBlock::class
        ];
    }
}
