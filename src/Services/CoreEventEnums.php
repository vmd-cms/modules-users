<?php

namespace VmdCms\Modules\Users\Services;

class CoreEventEnums
{
    const USER_SIGN_IN = 'user.sign_in';
    const USER_SIGN_UP = 'user.sign_up';
    const USER_SIGN_UP_FAIL = 'user.sign_up_fail';
    const USER_LOGOUT = 'user.logout';
    const USER_RECOVERY_PASSWORD_EMAIL = 'user.recovery_password.email';
    const USER_RECOVERY_PASSWORD_EMAIL_CONFIRMED = 'user.recovery_password.email_confirmed';
    const USER_RECOVERY_PASSWORD_SMS = 'user.recovery_password.sms';
    const USER_RECOVERY_PASSWORD_SMS_CONFIRMED = 'user.recovery_password.sms_confirmed';
}
