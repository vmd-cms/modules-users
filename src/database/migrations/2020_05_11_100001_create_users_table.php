<?php

use App\Modules\Users\Models\User as model;
use App\Modules\Users\Models\UserGroup as modelForeign;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_group_id')->unsigned()->nullable();
            $table->string('email',128)->unique()->nullable();
            $table->string('phone',50)->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('photo')->nullable();
            $table->string('role')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('ip_address')->nullable();
            $table->text('data')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('confirmed')->default(false);
            $table->float('discount_percent')->unsigned()->default(0);
            $table->string('provider',128)->nullable();
            $table->string('provider_id',128)->nullable();
            $table->string('comments',1024)->nullable();
            $table->string('source_id',128)->nullable();
            $table->string('import_source_id',128)->nullable();
            $table->boolean('deleted')->default(false);
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('user_group_id', model::table() . '_user_group_id_fk')->references('id')->on(modelForeign::table())->onUpdate('CASCADE')->onDelete('SET NULL');
        });
        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('deleted_by', model::table() . '_deleted_by_fk')->references('id')->on(\VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::table())->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
