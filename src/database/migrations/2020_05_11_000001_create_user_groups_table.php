<?php

use App\Modules\Users\Models\UserGroup as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug',32)->unique();
            $table->string('title',128)->nullable();
            $table->string('description',512)->nullable();
            $table->float('discount_percent')->unsigned()->default(0);
            $table->integer('order')->unsigned()->default(1);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
