<?php

use VmdCms\Modules\Users\Models\UserSession as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('session_id',128)->nullable();
            $table->string('token',128)->unique();
            $table->string('ip',64)->nullable();
            $table->string('device',64)->nullable();
            $table->string('browser',64)->nullable();
            $table->string('platform',64)->nullable();
            $table->integer('requests')->default(0);
            $table->boolean('compromised')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamp('expires')->nullable();
            $table->timestamps();
        });
        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('user_id', model::table() . '_user_id_fk')->references('id')->on(\VmdCms\Modules\Users\Models\User::table())->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
