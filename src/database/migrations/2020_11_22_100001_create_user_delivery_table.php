<?php

use App\Modules\Users\Models\UserDelivery as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('order_param_id')->unsigned()->nullable();
            $table->string('city_title',64)->nullable();
            $table->string('city_ref',64)->nullable();
            $table->string('city_delivery_id',64)->nullable();
            $table->string('warehouse_title',64)->nullable();
            $table->string('warehouse_ref',64)->nullable();
            $table->text('data')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('user_id', model::table() . '_user_id_fk')->references('id')->on(\App\Modules\Users\Models\User::table())->onUpdate('CASCADE')->onDelete('CASCADE');
         //   $table->foreign('order_param_id', model::table() . '_order_param_id_fk')->references('id')->on(\App\Modules\Orders\Models\Params\DeliveryTypeParam::table())->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
