<?php

namespace VmdCms\Modules\Users\database\seeds;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Users\Sections\User;
use App\Modules\Users\Sections\Subscriber;
use App\Modules\Users\Sections\UserGroup;
use App\Modules\Users\Sections\Components\CabinetMenu;
use App\Modules\Users\Sections\Components\CabinetGroupBlock;
use App\Modules\Users\Sections\Components\AuthGroupBlock;
use App\Modules\Users\Sections\Components\CabinetTranslate;
use App\Modules\Users\Sections\Components\AuthTranslate;

class ModuleSeeder extends AbstractModuleSeeder
{
    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            User::class => CoreLang::get('customers'),
            UserGroup::class => CoreLang::get('customers_groups'),
            Subscriber::class => CoreLang::get('subscribers'),
            CabinetMenu::class => CoreLang::get('cabinet_menu'),
            CabinetGroupBlock::class => CoreLang::get('info_blocks_cabinet'),
            AuthGroupBlock::class => CoreLang::get('info_blocks_auth'),
            CabinetTranslate::class => CoreLang::get('translates_cabinet'),
            AuthTranslate::class => CoreLang::get('translates_auth'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "users_group",
                "title" => CoreLang::get('users'),
                "is_group_title" => true,
                "children" => [
                    [
                        "slug" => "users",
                        "title" => CoreLang::get('customers'),
                        "section_class" => User::class,
                        "icon" => "icon icon-user"
                    ],
                    [
                        "slug" => "user_groups",
                        "title" => CoreLang::get('customers_groups'),
                        "section_class" => UserGroup::class,
                        "icon" => "icon icon-user"
                    ],
                    [
                        "slug" => "subscribers",
                        "title" => CoreLang::get('subscribers'),
                        "section_class" => Subscriber::class,
                        "icon" => "icon icon-user"
                    ]
                ]
            ],
            [
                "slug" => "content_group",
                "title" => CoreLang::get('content'),
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "menu_groups",
                        "title" => CoreLang::get('menu'),
                        "icon" => "icon icon-menu-burger",
                        "children" => [
                            [
                                "slug" => "cabinet_menu",
                                "title" => CoreLang::get('cabinet_menu'),
                                "section_class" => CabinetMenu::class,
                                "order" => 3
                            ],
                        ]
                    ],
                    [
                        "slug" => "block_groups",
                        "title" => CoreLang::get('info_blocks'),
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "cabinet_blocks",
                                "title" => CoreLang::get('cabinet'),
                                "section_class" => CabinetGroupBlock::class,
                                "order" => 3,
                            ],
                            [
                                "slug" => "auth_blocks",
                                "title" => CoreLang::get('auth'),
                                "section_class" => AuthGroupBlock::class,
                                "order" => 4,
                            ]
                        ]
                    ]
                ]
            ],
            [
                "slug" => "configs",
                "title" => CoreLang::get('configurations'),
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "translate_groups",
                        "title" => CoreLang::get('translates'),
                        "order" => 2,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "cabinet_translates",
                                "title" => CoreLang::get('cabinet'),
                                "section_class" => CabinetTranslate::class,
                                "order" => 4
                            ],
                            [
                                "slug" => "auth_translates",
                                "title" => CoreLang::get('auth'),
                                "section_class" => AuthTranslate::class,
                                "order" => 5
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function seedModelData()
    {
        $this->createItem('default', 'Клиенты', 1);
        $this->createItem('regular', 'Постоянные клиенты', 2, 10);
        $this->createItem('wholesale', 'Оптовые клиенты', 3, 20);
        return;
    }

    protected function createItem($slug, $title, $order, $discount = 0)
    {
        $model = new \App\Modules\Users\Models\UserGroup();
        $model->slug = $slug;
        $model->title = $title;
        $model->discount_percent = $discount;
        $model->order = $order;
        $model->active = true;
        $model->save();

        $modelInfo = new \App\Modules\Users\Models\UserGroupInfo();
        $modelInfo->user_groups_id = $model->id;
        $modelInfo->title = $title;
        $modelInfo->save();
    }
}
