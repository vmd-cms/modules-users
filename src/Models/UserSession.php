<?php

namespace VmdCms\Modules\Users\Models;

use VmdCms\CoreCms\Models\CmsModel;

class UserSession extends CmsModel
{
    public static function table(): string
    {
        return 'user_sessions';
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
