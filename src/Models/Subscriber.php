<?php

namespace VmdCms\Modules\Users\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;

class Subscriber extends CmsModel implements ActivableInterface
{
    use Activable;

    public static function table(): string
    {
       return 'subscribers';
    }
}
