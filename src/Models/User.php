<?php

namespace VmdCms\Modules\Users\Models;

use App\Modules\Orders\Models\Params\DeliveryTypeParam;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\SoftDeletableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasImagePath;
use VmdCms\CoreCms\Traits\Models\SoftDeletable;

class User extends CmsModel implements Authenticatable,SoftDeletableInterface,ActivableInterface
{
    use \Illuminate\Auth\Authenticatable, SoftDeletable, Activable, HasImagePath;

    protected $fillable = ['name','email','password', 'users_tole_id', 'has_full_access', 'active'];

    public static function table(): string
    {
        return "users";
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getFullNameAttribute()
    {
        return implode(' ',array_merge(
                [$this->attributes['last_name'] ?? ''],
                [$this->attributes['first_name'] ?? ''],
                [$this->attributes['father_name'] ?? '']
            )
        );
    }

    public function setFullNameAttribute($value)
    {
        $segments = explode(' ',$value);
        $this->attributes['first_name'] = $segments[0] ?? null;
        $this->attributes['last_name'] = $segments[1] ?? null;
        $this->attributes['father_name'] = $segments[2] ?? null;
    }

    public function setPasswordHashAttribute($value)
    {
        if(!empty($value)) $this->attributes['password'] = $value;
    }

    public function getPasswordHashAttribute()
    {
        return null;
    }

    public function getIsActiveAttribute()
    {
        return $this->attributes['active'] ? 'true' : 'false';
    }

    public function setIsActiveAttribute($value)
    {
        $this->attributes['active'] =  $value === 'true';
    }

    public function userGroup(){
        return $this->belongsTo(UserGroup::class,'user_group_id','id');
    }

    public function userDelivery(){
        return $this->hasMany(UserDelivery::class,'user_id','id');
    }

    public function delivery(){
        return $this->hasManyThrough(DeliveryTypeParam::class,UserDelivery::class,'user_id','id','id','order_param_id');
    }

    /**
     * @return string|null
     */
    protected static function getGroupKey(): ?string
    {
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getGroupKey())
        {
            static::addGlobalScope('group', function (Builder $builder) {
                $builder->whereHas('userGroup', function ($q){
                    $q->where('slug',static::getGroupKey());
                });
            });
            static::saving(function (User $model){
                $group = UserGroup::where('slug',static::getGroupKey())->first();
                if($group instanceof UserGroup){
                    $model->user_group_id = $group->id;
                }

            });
        }
    }
}
