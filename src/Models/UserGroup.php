<?php

namespace VmdCms\Modules\Users\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;

class UserGroup extends CmsModel implements ActivableInterface, HasInfoInterface
{
    use Activable,HasInfo;

    public static function table(): string
    {
        return 'user_groups';
    }


    private function getInfoClass() : string
    {
        return UserGroupInfo::class;
    }
}
