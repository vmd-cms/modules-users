<?php

namespace VmdCms\Modules\Users\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class UserGroupInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'user_groups_info';
    }
}
