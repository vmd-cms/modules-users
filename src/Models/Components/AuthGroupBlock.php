<?php

namespace VmdCms\Modules\Users\Models\Components;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class AuthGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'auth';
    }
}
