<?php

namespace VmdCms\Modules\Users\Models\Components;

use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\Menu\Menu;
use VmdCms\CoreCms\Exceptions\Models\NotCmsModelException;

class CabinetMenu extends Menu
{
    public static function getModelKey()
    {
        return 'cabinet_menu';
    }

    /**
     * @return string|null
     * @throws NotCmsModelException
     */
    public function getUrlPathAttribute(): ?string
    {
        if (!$this instanceof CmsModelInterface) throw new NotCmsModelException();

        $path = '/cabinet';
        if($this->url) $path .= '/' . $this->url;
        return $path;
    }
}
