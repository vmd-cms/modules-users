<?php

namespace VmdCms\Modules\Users\Models\Components;

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;

class CabinetTranslate extends Translate
{
    public static function getModelGroup(): ?string
    {
        return 'cabinet';
    }
}
