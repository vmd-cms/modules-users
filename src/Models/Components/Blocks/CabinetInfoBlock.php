<?php

namespace VmdCms\Modules\Users\Models\Components\Blocks;

use VmdCms\Modules\Users\Models\Components\CabinetGroupBlock;
use VmdCms\Modules\Users\Services\BlockEnum;

class CabinetInfoBlock extends CabinetGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::CABINET_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title'];
    }
}
