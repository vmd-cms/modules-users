<?php

namespace VmdCms\Modules\Users\Models\Components\Blocks;

use VmdCms\Modules\Users\Models\Components\AuthGroupBlock;
use VmdCms\Modules\Users\Services\BlockEnum;

class LogInInfoBlock extends AuthGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::LOGIN_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title','link','link_title'];
    }
}
