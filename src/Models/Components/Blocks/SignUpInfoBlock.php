<?php

namespace VmdCms\Modules\Users\Models\Components\Blocks;

use VmdCms\Modules\Users\Services\BlockEnum;
use VmdCms\Modules\Users\Models\Components\AuthGroupBlock;

class SignUpInfoBlock extends AuthGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::SIGNUP_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title','link','link_title','benefits_title'];
    }
}
