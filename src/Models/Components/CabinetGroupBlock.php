<?php

namespace VmdCms\Modules\Users\Models\Components;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class CabinetGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'cabinet';
    }
}
