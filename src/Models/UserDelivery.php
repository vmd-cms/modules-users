<?php

namespace VmdCms\Modules\Users\Models;

use App\Modules\Orders\Models\Params\DeliveryTypeParam;
use VmdCms\CoreCms\Models\CmsModel;

class UserDelivery extends CmsModel
{
    public static function table(): string
    {
        return 'user_delivery';
    }

    public function delivery(){
        return $this->belongsTo(DeliveryTypeParam::class,'order_param_id','id');
    }
}
