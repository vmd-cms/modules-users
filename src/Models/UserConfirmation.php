<?php

namespace VmdCms\Modules\Users\Models;

use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;

class UserConfirmation extends CmsModel implements ActivableInterface
{
    use Activable;

    const EMAIL = 'email';
    const PHONE = 'phone';

    public static function table(): string
    {
        return 'user_confirmations';
    }

    protected static function boot()
    {
        parent::boot();

        self::saving(function (CmsModel $model){
            if($model->wasChanged('activated') && $model->activated === true)
            {
                $model->activated_at = date('Y-m-d H:i:s');
            }
        });
    }
}
